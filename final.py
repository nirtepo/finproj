# -*- coding: utf-8 -*-
'''
UFFS - Universidade Federal da Fronteira Sul
Autor: Guilherme Antunes da Silva
Trabalho Final de GEX623 - Tópicos Especiais em Computação I - Ciência da Computação - 8ª Fase - Noturno - 2018/1
'''

from sklearn import datasets as dt
from sklearn import tree
from matplotlib import pyplot as plt
from sklearn.metrics import classification_report#accuracy_score, precision_score, recall_score, f1_score
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.externals.six import StringIO
from sklearn.feature_selection import SelectKBest, f_classif
import graphviz
import numpy as np
import pandas as pd
from collections import Counter
from pathlib import Path
from tempfile import TemporaryFile
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier

'''
	Explicação do Passo a Passo:
-> O primeiro passo foi tratar as colunas que tinham valores faltantes (representados por ?). O metodo utilizado foi o mais rústico, que é excluir todas as colunas que possuiam esses valores faltantes. Com isso, de 50 colunas passamos a ter 4*. Esse passo foi efetuado com a função dropna do pandas.
-> Em seguida tive de discretizar os valores das colunas que não possuiam valores exclusivamente numéricos (incluindo as faixas de valores). Esse passo é efetuado na função "discretizaColuna" apenas para as colunas que possuem valores que retornam false para a função "isNumber"
-> Como os passos anteriores são muito demorados, eu salvo o resultado no arquivo "temp.npy" para agilizar o debug das próximas etapas (eu utilizava isso durante a codificação)
-> Em seguida o y recebe a última coluna e o X as demais colunas.
-> Então utiliza-se a função KBest para escolher dentre os 4* atributos de X, os 10 melhores para criação do modelo.
-> Em seguida X e y são divididos em conjuntos de treinamento(90%) e teste(10%)

-> E por fim, com a ajuda do GridSearchCV, é feito a iteração para os 3 classificadores: K Neighbors, Decision Tree e Multi-layer Perceptron. Para cada um deles é definida uma lista de 3 hiperparâmetros, cada um com no mínimo 2 valores. O GridSearchCV trabalha com 5 fatias, sendo isso definido no atributo "cv". Durante cada iteração, o resultado é salvo respectivamente nos arquivos temp0.ny, temp1.ny, temp2.ny (em virtude da demora para cada a execução de cada classificador: foi em torno de 8 minutos para o K Neighbors, 40 segundos para o DecisionTree). A métrica utilizada para a performance foi o classification_report.


'''
 
def isNumber(a):
	b = str(a)
	return b.isdigit()

def discretizaColuna(dados):
	aux = np.unique(dados)
	ffff = {aux[d]:d+1 for d in range (aux.shape[0])}
	t = np.zeros(dados.shape[0])
	for d in range (dados.shape[0]):
		t[d] = ffff[dados[d]]
	return t

#Download and Install
#pip install graphviz
#sudo apt-get install graphviz

myfile=Path("temp.npy")
if(not myfile.is_file()):
	data = pd.read_csv('diabetic_data.csv')
	data = data.replace('?', np.NaN)
	data = data.dropna(axis=1)
	for i in range(data.shape[1]):
		agor = data[data.columns[i]]
		un = np.unique(agor)
		if(not isNumber(un[0])):
			data[data.columns[i]] = discretizaColuna(agor)

	
	np.save(myfile, np.array(data))
	print(data)
else:
	data = np.load(myfile)
	print(data)
X = ((data.T)[:-1]).T
y = ((data.T)[-1]).T
sel = SelectKBest(f_classif, k=10)
X=sel.fit_transform(X, y)
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.10)
print(X.shape)
classifiers = [
    KNeighborsClassifier, DecisionTreeClassifier,MLPClassifier]
classifiersLabels = ['K Neighbors', 'Decision Tree', 'Multi-layer Perceptron Classifier']
parameters = [{'n_neighbors':[3,4,5], 'weights':['uniform', 'distance'], 'algorithm':['kd_tree', 'ball_tree']},{'max_depth':[3,4,5, 40], 'splitter':['best','random' ], 'criterion':['gini', 'entropy']},{'alpha':[1,2,3]}]

for i in range (3):
	myfile=Path("temp"+str(i)+".npy")
	if(not myfile.is_file()):
		clf = GridSearchCV(classifiers[i](),parameters[i], cv=5)
		clf.fit(X_train, y_train)
		y_hat = clf.predict(X_test)
		np.save(myfile, y_hat)
	else:
		y_hat = np.load(myfile)
	'''neigh = KNeighborsClassifier(3)
	neigh.fit(X_train,y_train)
	y_hat = neigh.predict(X_test)'''
	print(classifiersLabels[i],":\n", classification_report(y_test, y_hat))
